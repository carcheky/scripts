# scripts

[![pipeline status](https://gitlab.com/carcheky/scripts/badges/main/pipeline.svg)](https://gitlab.com/carcheky/scripts/-/commits/main)

## zsh

```
# ZSH
sudo apt update && sudo apt install -y wget sudo curl
wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-zsh.sh | bash
chsh -s /usr/bin/zsh
zsh
```

## docker-ce

```
# DOCKER-CE
wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-docker-ce.sh | bash
sudo usermod -aG docker $USER
```

## ddev

```
# DOCKER ddev
wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-docker-ddev.sh | bash
```

## config wsl to no sudo password

```
# No sudo password on wsl
wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/wsl-conf.sh | bash
```

## all
```
sudo rm -fr .z* .oh* ;
sudo apt update && sudo apt upgrade -y && sudo apt install -y wget sudo curl
wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-zsh.sh | bash
chsh -s /usr/bin/zsh
wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-docker-ce.sh | bash
wget -O - https://gitlab.com/carcheky/scripts/-/raw/main/install/install-docker-ddev.sh | bash
```
