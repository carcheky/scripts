#!/bin/bash

/usr/local/bin/code tunnel service uninstall
sudo rm -fr /usr/local/bin/code

if [[ $(uname -m) == "x86_64" ]]; then
    curl -Lk 'https://code.visualstudio.com/sha/download?build=stable&os=cli-alpine-x64' --output vscode_cli.tar.gz ;
elif [[ $(uname -m) == "armv7l" ]]; then
    curl -Lk 'https://code.visualstudio.com/sha/download?build=stable&os=cli-linux-armhf' --output vscode_cli.tar.gz ;
else
    curl -Lk 'https://code.visualstudio.com/sha/download?build=stable&os=cli-alpine-arm64' --output vscode_cli.tar.gz ;
fi

tar -xf vscode_cli.tar.gz
sudo mv code /usr/local/bin
rm vscode_cli.tar.gz
/usr/local/bin/code tunnel service install
sudo loginctl enable-linger $USER
/usr/local/bin/code tunnel rename $(hostname -f)
echo "CODE CLI INSTALLED"
