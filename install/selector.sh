#!/bin/bash

echo "version: 1.1"
if [ -z "$1" ]; then
    echo "No argument supplied"
    # show menu to choose between "code-cli ddev docker gitlab k8s zsh"
    echo "select option:" 
    echo "1. code-cli"
    echo "2. ddev"
    echo "3. docker"
    echo "4. gitlab"
    echo "5. k8s"
    echo "6. zsh"
    read -r option

    case $option in
        1) option="code-cli" ;;
        2) option="ddev" ;;
        3) option="docker" ;;
        4) option="gitlab" ;;
        5) option="k8s" ;;
        6) option="zsh" ;;
        *) echo "Invalid option"; exit 1 ;;
    esac

    [ -n "$option" ] && wget -O - "https://gitlab.com/carcheky/scripts/-/raw/main/install/install-${option}.sh" | bash || echo "invalid option"
else
    echo "Argument: $1"
    wget -O - "https://gitlab.com/carcheky/scripts/-/raw/main/install/install-${1}.sh" | bash
fi
