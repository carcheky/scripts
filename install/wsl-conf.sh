#!/bin/bash

sudo echo "[boot]
systemd=true" > wsl.conf
sudo echo "user ALL=(ALL) NOPASSWD: ALL" > user
sudo mv wsl.conf /etc/wsl.conf
sudo mv user /etc/sudoers.d/user
sudo chown 0:0 /etc/sudoers.d/user

cat  /etc/wsl.conf
cat  /etc/sudoers.d/user

exit
