#!/bin/bash

read password

sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates perl

# sudo apt-get install -y postfix

curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

sudo GITLAB_OMNIBUS_CONFIG="letsencrypt['enable'] = false" GITLAB_ROOT_PASSWORD="${password:-cipassword}" EXTERNAL_URL="https://gitlab.cckdev.es" apt-get install -y gitlab-ee
sudo GITLAB_OMNIBUS_CONFIG="letsencrypt['enable'] = false" GITLAB_ROOT_PASSWORD="${password:-cipassword}" EXTERNAL_URL="https://gitlab.cckdev.es" gitlab-ctl reconfigure
sudo gitlab-ctl restart