#!/bin/bash

if [[ $(which sudo) = '' ]]; then
    echo "sudo not found, installing..."
    apt update
    apt install sudo -y
fi

sudo apt update


echo 'Installing docker'
curl -fsSL https://get.docker.com | sudo sh
curl -fL https://raw.githubusercontent.com/docker/compose-switch/master/install_on_linux.sh | sudo sh
sudo systemctl enable docker
sudo systemctl start docker
sudo systemctl restart systemd-networkd.service
sudo groupadd docker
sudo usermod -aG docker ${USER}
docker --version
docker compose version


