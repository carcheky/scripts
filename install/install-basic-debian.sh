#!/bin/bash
set -eux
sudo apt update
sudo apt upgrade -y
sudo rm -fr .oh-my-zsh .z* /root/.oh-my-zsh &>/dev/null; 
bash ~/dotfiles/scripts/install/install-docker.sh
bash ~/dotfiles/scripts/install/install-ddev.sh
bash ~/dotfiles/scripts/install/install-zsh.sh
bash ~/dotfiles/scripts/install/wsl-conf.sh
bash ~/dotfiles/setup.sh
sudo rm -fr /root/.oh-my-zsh ; 
sudo reboot